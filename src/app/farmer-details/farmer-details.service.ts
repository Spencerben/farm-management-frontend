
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { ApiService } from '../services';

@Injectable()

export class FarmerDetailsService {
	// set the API Url
  id: number;
  private _url = '/v1/farmers';

  constructor(  private apiService: ApiService) { }

  getFarmer(farmer_entity_id: any): Observable<any> {
    return this.apiService.get(this._url + '/' + farmer_entity_id);
  }
  deleteFarmer(farmer_entity_id: any): Observable<any> {
    return this.apiService.delete(`${this._url}/${farmer_entity_id}`);
  }

}


