import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FarmerDetailsService } from './farmer-details.service';
import { Farm, } from '../models';
import { FarmService } from '../farm/farm.service';
@Component({
  selector: 'app-farmer-details',
  templateUrl: './farmer-details.component.html',
  styleUrls: ['./farmer-details.component.scss']
})

export class FarmerDetailsComponent implements OnInit {
  id: number;
  farmerDetails: any;
  image= '/assets/images/profile.jpg';
  loading = true;
  constructor( 
      private route:ActivatedRoute, 
      private _farmerDetailsService:FarmerDetailsService,
      private farmsService: FarmService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    this.getFarmerDetails(this.id);

  }

  getFarmerDetails(id: number): any {

    this._farmerDetailsService.getFarmer(id)
    .subscribe((data: any) => { this.farmerDetails = data;
     this.loading = false;
   } );

  }

  delete(id:number){
    if (confirm("Are you sure you want to Delete this farm?")) {
      this.farmsService.deleteFarm(id)
      .subscribe(response => {
        if(!response){
          this.getFarmerDetails(this.id);
        }
      })
    }
  }
}
