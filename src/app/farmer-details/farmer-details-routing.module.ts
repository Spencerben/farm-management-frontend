import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { FarmerDetailsComponent } from './farmer-details.component';


const routes: Routes = Route.withShell([
  { path: 'farmerdetails/:id', component: FarmerDetailsComponent, data: { title: 'Edit Farmer' }}

]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class FarmerDetailsRoutingModule { }
