import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { FarmerDetailsRoutingModule } from './farmer-details-routing.module';
import { FarmerDetailsService } from './farmer-details.service';
import { FarmerDetailsComponent } from './farmer-details.component';

@NgModule({
  imports: [
    CommonModule, FarmerDetailsRoutingModule , FormsModule , ReactiveFormsModule 
  ],
  declarations: [FarmerDetailsComponent],
  providers: [FarmerDetailsService]
})
export class FarmerDetailsModule { }
