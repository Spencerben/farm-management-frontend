import { Component, Input } from '@angular/core';

import { Farm, FarmListConfig } from '../../models';
import { FarmService } from '../farm.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'farm-list',
  templateUrl: './farm-list.component.html'
})
export class FarmListComponent {
  constructor (
    private farmsService: FarmService,
    private alertService: AlertService,
    ) { }

  @Input() limit: number;
  @Input()
  set config(config: FarmListConfig) {
    if (config) {
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  query: FarmListConfig;
  farms: Farm[]= [];
  loading = false;
  moreFarms = true;
  currentPage = 1;
  // farmsCount = 15;
  // ttlpages:number = 0;
  totalPages: Array<number> = [1];
  cursor = '';

  // setPageTo(pageNumber: number) {
  //   this.currentPage = pageNumber;
  //   this.runQuery();
  // }

  getNext() {
      this.loading = true;
      this.query.filters.limit = 5;
      this.query.filters.cursor = this.cursor;
      this.farmsService.query(this.query)
          .subscribe(
            data => {
              this.loading = false;
              if (data.items){
                this.farms = this.farms.concat(data.items);
              } else {
                  this.moreFarms = false;
              }
              this.cursor = data.nextPageToken;
          });
  }

  runQuery() {
    this.loading = true;
    this.farms = [];

    // Create limit and offset filter (if necessary)
    if (this.limit) {
      this.query.filters.limit = this.limit;
      // this.query.filters.offset =  (this.limit * (this.currentPage - 1));
    }
 
    this.farmsService.query(this.query)
    .subscribe(
      data => {
        this.loading = false;
        // console.log("The farms are " + JSON.stringify(data.items));
        if (data.items){
          this.farms = data.items;
          // this.ttlpages += 1;
        } 
        this.cursor = data.nextPageToken;
        // Used from http://www.jstips.co/en/create-range-0...n-easily-using-one-line/
        // this.totalPages = Array.from(new Array(Math.ceil(data.farmsCount / this.limit)), (val, index) => index + 1);
    },
    error => {
      this.alertService.error(JSON.stringify(error));
      this.loading = false;
      this.farms=[];
    });
  }

  delete(farm:Farm){
    if (confirm("Are you sure you want to Delete " + farm.name + "?")) {
      this.farmsService.deleteFarm(farm.id)
      .subscribe(response => {
        if(!response){
          this.alertService.success("Farm '"+ farm.name + "' Deleted Successfully", true);
          this.runQuery();
        }
      })
    }
  }
  
}
