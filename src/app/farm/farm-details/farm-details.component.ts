import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Farm, Farmer } from '../../models';
import { AlertService } from '../../services/index';
import { FarmService } from '../farm.service';

@Component({
  selector: 'app-farm-details',
  templateUrl: './farm-details.component.html',
  styleUrls: ['./farm-details.component.css']
})
export class FarmDetailsComponent implements OnInit {
   title: string;
   label: string;
   farm: Farm = new Farm();
   public loading = false;
   farmowner: string = '';
   id: number;
   private sub: any;
   farmerId: number;
   farmer: Farmer = new Farmer();
   zoom: number = 16;

  constructor(
      private route: ActivatedRoute,
      private farmsService: FarmService,
      private alertService: AlertService
    ) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'] ? +params['id'] : null;

      if (!this.id)
        return;
      this.loading = true;
      this.farmsService.getFarm(this.id)
      .subscribe(
        farm => {
          this.farm = farm;
          this.loading = false;
        },
        error => {
          if (error.status == 404) {
            this.alertService.error("Oops!! That farm does not exist!", true);
            // this.router.navigate(['farms']);
          }
        });

    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
