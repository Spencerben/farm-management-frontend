import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FarmService } from './farm.service';
import { AlertService } from '../services/index';
import { Farm, Farmer, FarmListConfig } from '../models'

import {SelectItem} from 'primeng/components/common/selectitem';

@Component({
    selector: 'app-farm',
    templateUrl: './farm.component.html',
    styleUrls: ['./farm.component.scss']
})

export class FarmComponent implements OnInit {

    displayDialog: boolean;
    farm: Farm = new Farm();
    selectedFarm: Farm;
    newFarm: boolean;
    farms: Farm[];
    loading: boolean;
    acntStatus: SelectItem[];
    nextPage: string;
    isDeleting = false;


    constructor(private farmService: FarmService,
        private alertService: AlertService, 
        private router: Router,) { }

    listConfig: FarmListConfig = new FarmListConfig();

    ngOnInit() {
        this.loading = true;
        this.farms = [];

        // this.farmService.getFarms()
        // .subscribe(
        //   (farms: any) => { 
        //   this.farms = farms.items;
        //   this.nextPage = farms.nextPageToken; 
        //   },
        //   error => {
        //   this.alertService.error(error);
        //   }
        // );
        
        // this.loading = false;

    }
    

    nextFarm() {
        this.farms = [];
        this.loading = true;
        this.farmService.getNext(this.nextPage)
        .subscribe(
          (farms: any) => { 
          this.farms = farms.items;
          this.nextPage = farms.nextPageToken; 
          },
          error => {
          this.alertService.error(error);
          }
        );
        this.loading = false;
    }
    
    delete(farm: Farm) {
        this.isDeleting = true;
        if (confirm("Are you sure you want to Delete " + farm.name + "?")) {
            this.farmService.deleteFarm(farm.id)
            .subscribe(
              success => {
              this.router.navigateByUrl('/');
              },
              error => {
              this.alertService.error(error);
              }
            );
        }
        
    }    
    
}