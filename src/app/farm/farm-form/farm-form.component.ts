import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Farm, Farmer } from '../../models';
import { AlertService } from '../../services/index';
import { FarmService } from '../farm.service';
import { CountiesService } from '../../services/counties.service';

@Component({
  selector: 'app-farm-form',
  templateUrl: './farm-form.component.html',
  styleUrls: ['./farm-form.component.css']
})
export class FarmFormComponent implements OnInit, OnDestroy {

  form: FormGroup;
  title: string;
  label: string;
  farm: Farm = new Farm();
  counties: any[] = [];
  public loading = false;
  isNewFarm = false;
  farmowner: string = '';
  id: number;
  private sub: any;
  farmerId: number;
  farmer: Farmer = new Farmer();
  
  stati : any[] = 
  [
  {"option": "ACTIVE"},
  {"option": "INACTIVE"},
  {"option": "DISABLED"}
  ];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private farmsService: FarmService,
    private alertService: AlertService,
    private countiesService: CountiesService,
    ) {}

  ngOnInit() {

    this.countiesService.getCounties()
    .subscribe((counties: any) => { 
      this.counties = counties.data;
    });

    this.form = this.formBuilder.group({
      name: ['Farm Name', Validators.compose([Validators.required, Validators.minLength(4)])],
      size: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]+([,.][0-9]+)?$')])],
      status: [],
      county: ['', [Validators.required]],
      latitude: ['',  Validators.compose([Validators.required, Validators.pattern('^[-+]?[0-9]{1,7}(\.[0-9]+)?$')])],
      longitude: ['',  Validators.compose([Validators.required, Validators.pattern('^[-+]?[0-9]{1,7}(\.[0-9]+)?$')])]
    });

    // If a farm ID is provided in the URL, get the farm details and render the Edit Form.
    this.sub = this.route.queryParams.subscribe(params => {
      this.id = params['id'] ? +params['id'] : null;
      this.farmerId = params['farmerId'] ? +params['farmerId'] : null;
      // this.title = this.id ? 'Edit This Farm' : 'Create New Farm';
      this.label = this.id ? 'Update Farm' : 'Register Farm';

      // if (!this.id)
      //   return;

      if (this.id){
        this.farmsService.getFarm(this.id)
        .subscribe(
          farm => this.farm = farm,
          response => {
            if (response.status == 404) {
              this.alertService.error("Oops!! That farm does not exist!", true);
              this.router.navigate(['farms']);
            }
          });
      }

      if (this.farmerId){
        this.farmsService.getFarmer(this.farmerId)
        .subscribe(
          farmer => { 
            this.farmer = farmer;
          },
          error => {
            this.alertService.error(error);
          }
          );
      };

    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  save() {
    var result,
    farmValue = this.form.value;

    // farmValue.farmOwner = { "id": this.farmowner}; 
    this.loading = true;

    if (this.farm.id){
      // means its an edit
      farmValue.id = this.farm.id;
      this.farmsService.updateFarm(farmValue)
      .subscribe(
        data => {
          this.location.back();
          // this.router.navigate(['farms']);
          this.alertService.success('Farm successful updated.', true);
          // this.addToFarmer(data.id);
        },
        error => {
          this.alertService.error(JSON.stringify(error));
          this.loading = false;
        }
        );
    } else {
      this.farmsService.addFarm(farmValue)
      .subscribe(
        data => {
          // 
          if(this.farmerId){
            this.addToFarmer(parseInt(data.id));
          } else {
            this.alertService.success('Farm with no Owner Successfully Added.', true);

            this.location.back();
            // this.router.navigate(['farms']);
          }
          // this.addToFarmer(data.id);
          // 
        },
        error => {
          this.alertService.error(JSON.stringify(error));
          this.loading = false;
        }
        );
    }

  }

  private addToFarmer(farmid: number){
    if(this.farmer.farms){
      this.farmer.farms.push({'id': farmid});
    } else{
      var farms = [], elmts = new Object();
      elmts['id'] = farmid;
      farms.push(elmts);
      this.farmer['farms'] = farms;
    }

    this.farmsService.updateFarmer(this.farmer)
    .subscribe(
      (farmer: any) => { 
        this.alertService.success("Successfully saved farm - ID = "+ farmid + " ! :-)", true );
        this.location.back();
        // this.router.navigate(['farms']);
      },
      error => {
        this.alertService.error(JSON.stringify(error));
      }
      );
  }
}