import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { FarmComponent } from './farm.component';
import { FarmListComponent } from './farm-list/farm-list.component';
import { FarmFormComponent } from './farm-form/farm-form.component';
import { FarmDetailsComponent } from './farm-details/farm-details.component';


const routes: Routes = Route.withShell([
  { path: 'farms', component: FarmComponent, data: { title: 'Farms' } },
  { path: 'farm/new', component: FarmFormComponent, data: { title: 'Add New Farm' }},
  { path: 'farm/edit', component: FarmFormComponent, data: { title: 'Edit Farm' }},
  { path: 'farm/details/:id', component: FarmDetailsComponent, data: { title: 'View Farm Details' }},
  { path: 'farm/:id', component: FarmComponent, data: { title: 'View Farm' }},
  { path: 'farm/delete/:id', component: FarmListComponent, data: { title: 'Deleting a Farm' }}
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class FarmRoutingModule { }