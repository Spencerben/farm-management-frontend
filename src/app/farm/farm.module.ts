import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { SchemaFormModule, WidgetRegistry, DefaultWidgetRegistry } from "angular2-schema-form";
import { FarmRoutingModule } from './farm-routing.module';
import { FarmComponent } from './farm.component';
import { FarmFormComponent} from './farm-form/farm-form.component';
import { FarmListComponent } from './farm-list/farm-list.component';
import { FarmService } from './farm.service';
import { AgmCoreModule } from '@agm/core';
import { CountiesService } from '../services/counties.service';
import { FormsModule } from '@angular/forms';
import { MyDatePickerModule } from 'mydatepicker';
import { FarmDetailsComponent } from './farm-details/farm-details.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SchemaFormModule,
    FarmRoutingModule,
    ReactiveFormsModule,
    MyDatePickerModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCznaZpxy_38p7KhysIZ0V72ppnlk7ihC4'
    })
  ],
  declarations: [
    FarmComponent,
    FarmFormComponent,
    FarmListComponent,
    FarmDetailsComponent
  ],
  providers: [FarmService,CountiesService]
})
export class FarmModule { }
