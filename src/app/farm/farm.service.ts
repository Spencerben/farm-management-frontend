import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Farm, FarmListConfig, Farmer } from '../models';

import { ApiService, JwtService } from '../services';

@Injectable()
export class FarmService {

	private url: string = "/v1/farms";
	// private limit: string = "5";

	// allFarms:Array<any>;
	allData: any;

	constructor( 
		private jwtService: JwtService,
		private apiService: ApiService) {}

	query(config: FarmListConfig): Observable<any> {
	  // Convert any filters over to Angular's URLSearchParams
	  const params: URLSearchParams = new URLSearchParams();

	  Object.keys(config.filters)
	  .forEach((key) => {
	    params.set(key, config.filters[key]);
	  });

	  return this.apiService
	  .get(
	    this.url,
	    params
	  ).map(data => data);
	}

	getFarms(): Observable<any> {

		// return this.apiService.get(this.url).map((response: Response) => response);

		return this.apiService.get(this.url).map((response: Response) => response);
		// .catch(() => Observable.of('Error, could not load Farms :-('));
	}

	getFarmer(id: number): Observable<Farmer> {
		return this.apiService.get(this.getFarmerUrl()+id);
		// .catch(() => Observable.of('Error, could not load Farms :-('));
	}

	getFarm(id: number): Observable<any>{
	  return this.apiService.get(this.getFarmUrl(id));
	}

	addFarm(farm: Farm): Observable<Farm>{
	  return this.apiService.post(this.url, farm);
	}

	updateFarm(farm: Farm): Observable<Farm>{
	  return this.apiService.put(this.getFarmUrl(farm.id), farm)
	    .map(res => res);
	}

	updateFarmer(farmer: Farmer): Observable<Farmer>{
	  return this.apiService.put(this.getFarmerUrl() + farmer.id, farmer)
	    .map(res => res);
	}

	deleteFarm(id: number): Observable<any>{
	  return this.apiService.delete(this.getFarmUrl(id))
	    .map(res => res);
	}

	getNext(id: string): Observable<any> {
	  return this.apiService.get(this.url)
	  .map((response: Response) => response);
	}

	private getFarmUrl(id: number){
	  return this.url +'/'+id;
	}

	private getFarmerUrl(){
	  return '/v1/farmers/';
	}
	
}
