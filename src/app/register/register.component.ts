import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';

import {EmailValidator, EqualPasswordsValidator} from '../shared/validators';
import { AlertService, UserService } from '../services/index';
import { User } from '../models/index';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  public form:FormGroup;
  public name:AbstractControl;
  // public phonenumber:AbstractControl;
  public email:AbstractControl;
  public password:AbstractControl;
  public repeatPassword:AbstractControl;
  public passwords:FormGroup;
  public loading = false;
  public model :any = {};
  preferredCountries = ['ke', 'ng'];
  phone_number = '';

  public submitted:boolean = false;

  constructor(fb:FormBuilder,
        private router: Router, 
        private userService: UserService,
        private alertService: AlertService) {

    this.form = fb.group({
      // 'name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'email': ['', Validators.compose([Validators.required, EmailValidator.validate])],
      // 'phonenumber': ['', Validators.required],
      'passwords': fb.group({
        'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
        'repeatPassword': ['', Validators.compose([Validators.required, Validators.minLength(4)])]
      }, {validator: EqualPasswordsValidator.validate('password', 'repeatPassword')})
    });

    // this.name = this.form.controls['name'];
    this.email = this.form.controls['email'];
    // this.phonenumber = this.phone_number;
    this.passwords = <FormGroup> this.form.controls['passwords'];
    this.password = this.passwords.controls['password'];
    this.repeatPassword = this.passwords.controls['repeatPassword'];
  }

  public onSubmit(values:Object):void {
    this.submitted = true;
    this.loading = true;
    if (this.form.valid) {
      this.model.phoneNumber = this.phone_number;
      this.userService.create(this.model)
          .subscribe(
              data => {
                  this.alertService.success('Registration successful. Please log in', true);
                  this.router.navigate(['/login']);
              },
              error => {
                  this.alertService.error(JSON.stringify(error), true);
                  this.loading = false;
              });
    }
  }
}
