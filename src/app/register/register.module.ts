import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { NgaModule } from '../../theme/nga.module';

import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    // NgaModule,
    RegisterRoutingModule,
    BsDropdownModule.forRoot(),
    NgxIntlTelInputModule
  ],
  declarations: [
    RegisterComponent
  ]
})
export class RegisterModule {}
