import { TestBed, inject } from '@angular/core/testing';

import { FarmerEditService } from './farmer-edit.service';

describe('FarmerEditService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FarmerEditService]
    });
  });

  it('should be created', inject([FarmerEditService], (service: FarmerEditService) => {
    expect(service).toBeTruthy();
  }));
});
