import { Component, Input, OnInit } from '@angular/core';
import  { FarmerComponent } from './../farmer/farmer.component';

@Component({
  selector: 'farmer-edit',
  templateUrl: './farmer-edit.component.html',
  styleUrls: ['./farmer-edit.component.scss']
})
export class FarmerEditComponent implements OnInit {
	@Input() fc: FarmerComponent;

  constructor() { }

  ngOnInit() {
  }

}
