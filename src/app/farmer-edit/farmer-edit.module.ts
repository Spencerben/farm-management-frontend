import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { FarmerEditRoutingModule } from './farmer-edit-routing.module';
import { FarmerEditService } from './farmer-edit.service';
import { FarmerEditComponent } from './farmer-edit.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class FarmerEditModule { }


