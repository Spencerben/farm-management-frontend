import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { FarmerEditComponent } from './farmer-edit.component';


const routes: Routes = Route.withShell([
  { path: 'farmeredit/:id', component: FarmerEditComponent, data: { title: 'Edit Farmer' }}

]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class FarmerEditRoutingModule { }
