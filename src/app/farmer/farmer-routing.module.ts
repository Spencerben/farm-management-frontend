import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { FarmerComponent } from './farmer.component';


const routes: Routes = Route.withShell([
  { path: 'farmer', component: FarmerComponent}

]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class FarmerRoutingModule { }
