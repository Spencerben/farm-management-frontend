import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Farmer } from './farmer'
import { Observable } from 'rxjs/Observable';
import { ApiService, JwtService } from '../services';

@Injectable()

export class FarmerService {
	//set the API Url
		id: number;
		private _url:string = '/v1/farmers';
    limit:number = 5;
   
 		 constructor(  private apiService: ApiService) { }
 
  getFarmer(): Observable<Array<any>>{
    return this.apiService.get(this._url+"?limit="+this.limit);
  }
  getMoreFarmers(nextPageTokenFarmers:string): Observable<Array<any>>{
    return this.apiService.get(this._url+"?limit="+this.limit+"&cursor="+nextPageTokenFarmers);

  }

  deleteFarmer(farmer: Farmer): Observable<Response>{
    return this.apiService.delete(this._url + '/' + farmer.id);
  }
  updateFarmer(farmer: Farmer): Observable<Array<any>>{
    return this.apiService.put(this._url + '/' + farmer.id , farmer);
  }
  addFarmer(farmer: Farmer): Observable<Array<any>>{
  	
     return this.apiService.post(this._url, farmer);
  }

}


