import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,FormControl , ReactiveFormsModule } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { FarmerRoutingModule } from './farmer-routing.module';
import { FarmerService } from './farmer.service';
import { FarmerComponent } from './farmer.component';
import { FarmerEditComponent } from '../farmer-edit/farmer-edit.component';



@NgModule({
  imports: [
    CommonModule, FormsModule , FarmerRoutingModule, ReactiveFormsModule 
  ],
  declarations: [
    FarmerComponent,FarmerEditComponent
  ],
  providers: [FarmerService]
})
export class FarmerModule { }
