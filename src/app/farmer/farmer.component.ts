import { Component, OnInit } from '@angular/core';
import { FarmerService } from './farmer.service';
import { Http, Response } from '@angular/http';
import { NgModule } from '@angular/core';
import {FormBuilder, FormGroup, Validators,FormControl  }   from '@angular/forms';
import { Farmer } from './farmer';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-farmer',
  templateUrl: './farmer.component.html',
  styleUrls: ['./farmer.component.scss']
})
export class FarmerComponent implements OnInit {
  form:{};
  farmers: Farmer[];
  id: number;
  key:string;
  msg:string;
  editFormOpen:boolean = false;
  success:boolean = false;
  error:boolean = false;
  addFarmerForm:boolean = false;

  



  notification(isSuccess:boolean, msg:string):void{

    if(isSuccess){
      this.success = isSuccess;
      this.error = !isSuccess;

    }else{
      this.success = false;
      this.error = true;
    }
    this.msg = msg;

  }

  //form fields
    firstName ='';
    lastName ='';
    phoneNumber = '';
    countryOfResidence = '';
     
      status : boolean;
      isLoadMore:boolean = true;
      d:{} = new Date();
    n:string = this.d.toString();
      date:string =this.n;
      nextPageToken:string;

      //for control
      farmersForm: FormGroup;
   



  constructor( private _farmerDetailsService:FarmerService, private fb: FormBuilder) {
    this.createFarmerForm();
  }

  ngOnInit() {

   this.getFarmers();

  }

  createFarmerForm(){
    
    this.farmersForm = this.fb.group ({
    firstName: ['', Validators.required ],
    lastName: ['', Validators.required ],
    countryOfResidence: ['', Validators.required ],
   /* email: ['',  Validators.compose([Validators.required,Validators.pattern("")]) ], */
    phoneNumber: ['', Validators.compose([Validators.required,Validators.minLength(5),Validators.pattern('^[0-9]+([,.][0-9]+)?$')]) ],
    /* status: '' */
  });

  }
  editFarmerForm(farmer?: Farmer){
   
    this.farmersForm = this.fb.group ({
      id: [farmer.id, Validators.required ],
      firstName: [farmer.firstName, Validators.required ],
      lastName: [farmer.lastName, Validators.required ],
      countryOfResidence: [farmer.countryOfResidence, Validators.required ],
      phoneNumber: [farmer.phoneNumber, Validators.required ]
     // phoneNumber: Validators.compose([farmer.phoneNumber, Validators.required, Validators.minLength(5),Validators.pattern('^[0-9]+([,.][0-9]+)?$')])
    /* status: '' */
  } ) ;

  }


  setNextPageToken(npt:string):void{
    this.nextPageToken = npt;
  }
  getNextPageTokenFarmers(){
    return this.nextPageToken;
  }
  getKey() {
      return this.key;
  }
   getFarmers(){
       this._farmerDetailsService.getFarmer()
          .subscribe((data: any) => { 
            this.farmers = data.items;
            this.setNextPageToken(data.nextPageToken); 
     
         } );
    }
  toggleForm(isActive:boolean = false, addFarmerForm?:boolean){
    if(isActive){
      this.editFormOpen = isActive;
    }
    else{
      this.editFormOpen = false;
    }
    if(addFarmerForm){
      this.addFarmerForm = addFarmerForm;
    }else{
      this.addFarmerForm = false;
    }

  }
  loadMore(){
    //loads more farmers and appends to farmers array based on limit + nextpage token
    this._farmerDetailsService.getMoreFarmers(this.getNextPageTokenFarmers())
  .subscribe((data: any) => { 
        if(data.items){
            this.farmers  = this.farmers.concat(data.items);
            this.setNextPageToken(data.nextPageToken);
        }else{
            this.isLoadMore = false;
            return false;
        } 
        
     
   } );
  }

  editFarmer(farmer: Farmer){
    this.editFarmerForm(farmer);
    this.id = farmer.id;
    this.firstName = farmer.firstName;
    this.lastName = farmer.lastName;
    this.phoneNumber =  farmer.phoneNumber;
    this.toggleForm(true);

  }
  addNewFarmerForm(farmer: Farmer) {
    this.createFarmerForm();
    this.firstName = '';
    this.lastName = '';
    this.phoneNumber =  '';
    this.status =  true;
    this.toggleForm(true, true);



  }
  addFarmer(){

 this._farmerDetailsService.addFarmer(this.farmersForm.value).subscribe((data: any) => {
  this.notification(true, 'the farmer details has been successfully Updated.');
   this.getFarmers(); } );
 this.toggleForm(false);
  }
  updateFarmer(farmer: Farmer ) {

  /* let farmerobj = {
    'id': this.id,
    "firstName": this.firstName,
    "lastName": this.lastName,
    "phoneNumber": this.phoneNumber,
    "countryOfResidence": this.countryOfResidence
  } */


 this._farmerDetailsService.updateFarmer(farmer).subscribe((data: any) => {
   this.notification(true, 'the farmer details has been successfully Updated.');
   this.getFarmers(); } );
 this.toggleForm(false);

  }


  deleteFarmer(farmer: Farmer) {


    // check if farmer has farms
    if (typeof farmer.farms !== 'undefined') {
      alert('You can not delete a farmer who has farms in their account');
    }else{
      // if no farms, then delete
      const feedback = confirm(' Are you sure you\'d like to delete this farmer?');
      if(feedback){
        this._farmerDetailsService.deleteFarmer(farmer)
        .subscribe((data: any) => {
              this.notification(true, 'the farmer has been successfully deleted.');
              this.getFarmers();
         } );
      }
    }
  }
}
