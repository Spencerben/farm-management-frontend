import { Injectable } from '@angular/core';


@Injectable()
export class JwtService {

  getToken() {
    var token = JSON.parse(window.localStorage.getItem('token'));
    return token ? token : null
  }

  saveToken(token: string) {
    window.localStorage.setItem('token', token);
  }

  destroyToken() {
    window.localStorage.removeItem('token');
  }

}
