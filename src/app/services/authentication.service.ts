import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { JwtHelper } from 'angular2-jwt';
import { JwtService } from './jwt.service';
import { ApiService } from './api.service';
import { tokenNotExpired } from 'angular2-jwt';

 
@Injectable()
export class AuthenticationService {
    loggedIn = false;
    isAdmin = false;

    jwtHelper: JwtHelper = new JwtHelper();

    currentUser = { _id: '', username: '', role: '' };

    constructor(private apiService: ApiService,
        private jwtService: JwtService,
        private router: Router) {

        const token = this.jwtService.getToken();
        if (token && this.isLoggedIn()) {
            
          const decodedUser = this.decodeUserFromToken(token);
          // console.log("The response from decodedUser is " + decodedUser);
          this.setCurrentUser(decodedUser);
        }
        else {
            this.logout();
        }
    }
    
    

    isLoggedIn() {
      return tokenNotExpired();
    }

    login(username: string, password: string): Observable<boolean> {
       return this.apiService.post('/v1/login', { email: username, password: password })
        .map(response => {
            // login successful if there's a jwt token in the response
            // let token = response.json();

            let token = response && response.token;
            if (token) {
                // set token property
                this.jwtService.saveToken(JSON.stringify(token));

                const decodedUser = this.decodeUserFromToken(token);
                this.setCurrentUser(decodedUser);

                // return true to indicate successful login
                return this.loggedIn;

            } else {
                // return false to indicate failed login
                return false;
            }
        });
    }
 
    logout():void {
        // remove user from local storage to log user out
        this.loggedIn = false;
        this.isAdmin = false;
        this.currentUser = { _id: '', username: '', role: '' };
        this.jwtService.destroyToken();
        this.router.navigate(['/login']);
    }

    decodeUserFromToken(token: string) {
      return this.jwtHelper.decodeToken(token).email;
    }

    setCurrentUser(decodedUser: any) {
      this.loggedIn = true;
      // this.currentUser._id = decodedUser._id;
      this.currentUser.username = decodedUser;
      // this.currentUser.role = decodedUser.role;
      // decodedUser.role === 'admin' ? this.isAdmin = true : this.isAdmin = false;
      delete decodedUser.role;
    }
}