import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { JwtService } from './jwt.service';

@Injectable()
export class ApiService {

  localparams: URLSearchParams = new URLSearchParams();
  
  constructor(
    private http: Http,
    private jwtService: JwtService
  ) {
    this.localparams.set('key', "AIzaSyCfeXZd87NupXe2E69fsEj2YiY8YOVvw98");
  }

  private setHeaders(): Headers {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (this.jwtService.getToken()) {
      headersConfig['Authorization'] = `Bearer ${this.jwtService.getToken()}`;
    }
    return new Headers(headersConfig);
  }

  private formatErrors(error: any) {
     return Observable.throw(error.json());
  }

  get(path: string, params: URLSearchParams = new URLSearchParams()): Observable<any> {
    params.set('key', "AIzaSyCfeXZd87NupXe2E69fsEj2YiY8YOVvw98");
    return this.http.get(`${path}`, { headers: this.setHeaders(), search: params })
    .catch(this.formatErrors)
    .map((res: Response) => res.json());
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(
      `${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders(), search: this.localparams }
    )
    .catch(this.formatErrors)
    .map((res: Response) => res.json());
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(
      `${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders(), search: this.localparams }
    )
    .catch(this.formatErrors)
    .map((res: Response) => res.json());
  }

  delete(path: string): Observable<any> {
    return this.http.delete(
      `${path}`,
      { headers: this.setHeaders(), search: this.localparams }
    )
    .catch(this.formatErrors)
    .map((res: Response) => res.json());
  }
}
