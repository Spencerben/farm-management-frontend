import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Observable} from "rxjs";

import * as allcounties from "../../assets/data/kenyacounties.json";

@Injectable()
export class CountiesService {
    
    constructor() {}

    getCounties() {
        return Observable.of(allcounties);
    }

    private handleError(error: any): Promise<any> {
      console.error('An error occurred', error);
      return Promise.reject(error.message || error);
    }
}
