import { Injectable } from '@angular/core';
// import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable'; 
import { User } from '../models/index';
import { ApiService } from './api.service';
 
@Injectable()
export class UserService {
    constructor(private apiService: ApiService) { }
 
    getAll() {
        return this.apiService.get('/api/users');
    }
 
    getById(id: number): Observable<User> {
        return this.apiService.get('/api/users/' + id);
    }
 
    create(user: User): Observable<User> {
        return this.apiService.post('/v1/users', user);
    }
 
    update(user: User) {
        return this.apiService.put('/api/users/' + user.id, user);
    }
 
    delete(id: number) {
        return this.apiService.delete('/api/users/' + id);
    }
 
}