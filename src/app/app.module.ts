import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule }   from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { LoginModule } from './login/login.module';
import { RegisterModule } from './register/register.module';

import { HomeModule } from './home/home.module';
import { AboutModule } from './about/about.module';

import { FarmerDetailsModule } from './farmer-details/farmer-details.module';
import { FarmerModule } from './farmer/farmer.module';
import { FarmModule } from './farm/farm.module';

import { UserService} from './services/index';

import { AlertComponent } from './directives/index';
import { AlertService} from './services/index';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { fakeBackendProvider } from './helpers/index';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';

// import {
//   FooterComponent,
//   HeaderComponent
// } from './shared';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    TranslateModule.forRoot(),
    NgbModule.forRoot(),
    CoreModule,
    SharedModule,
    LoginModule,
    RegisterModule,
    HomeModule,
    AboutModule, 
    FarmModule,
    FarmerModule,
    FarmerDetailsModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  declarations: [AppComponent,
    AlertComponent
    ],
  providers: [UserService,
    AlertService,
    // providers used to create fake backend
    // fakeBackendProvider,
    MockBackend,
    BaseRequestOptions
    ],

  bootstrap: [AppComponent]
})
export class AppModule { }
