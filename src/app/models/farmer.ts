import { Farm } from './farm';

export class Farmer{
	id:number;
	firstName:string;
	lastName:string;
	email?:string;
	phoneNumber:string;
	countryOfResidence:string;
	createdDate?:string;
	lastModifiedDate?:string;
	farms: Array<any> = [];

}