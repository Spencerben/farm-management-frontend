export class FarmListConfig {
  type = 'all';

  filters: {
    limit?: number,
    offset?: number,
    cursor?: string,
    nextpagetoken?: string
  } = {};
}
