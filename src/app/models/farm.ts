export class Farm {
	
		id?: any;
		name: string;
		latitude: number;
		longitude: number;
		size: number;
		status: boolean;
		farmOwner?: number;
		county?: string;
	
}