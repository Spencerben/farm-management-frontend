#!/usr/bin/env node

const faker = require('faker');
const seedrandom = require('seedrandom');

const SEED = 42;
const NUM_FARMS = 50;
const NUM_FARMERS = 50;

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomArbitrary(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomBoolean() {
  return Math.round(Math.random());
}

/**
 * Convert a Date ISOString to a compact yyyy-mm-dd representation
 * @returns {string}
 */
function getRandomDate() {
  const date = new Date(faker.date.past());
  return date.getFullYear() + '-' +
         (date.getUTCMonth() + 1) + '-' +
         date.getUTCDate();
}

function generateFakeFarms(size) {
  let farms = [];

  for (let i = 0; i < size; i++) {
    farms.push(
      {
        id: i,
        name: faker.name.findName(),
        coordinates: {
          latitude: getRandomArbitrary(0, 5),
          longitude: getRandomArbitrary(34, 42),
        },
        sizeAcres: getRandomInt(5, 30),
        county: faker.address.streetAddress(),
        status: getRandomBoolean(),
        date: getRandomDate()
      }
    );
  }
  return farms;
}

function generateFakeFarmers(size) {
  let farmers = [];

  for (let i = 0; i < size; i++) {
    farmers.push(
      {
        id: (i+1),
        firstname: faker.name.firstName(),
        lastname: faker.name.lastName(),
        email: faker.internet.email(),
        status: getRandomBoolean(),
        farms: getRandomBoolean(),
        phone: faker.phone.phoneNumber(),
        date: getRandomDate()
      }
    );
  }
  return farmers;
}

// Setting the seed always produces the same output
seedrandom(SEED, { global: true });
faker.seed(SEED);

module.exports = () => {
  return {
    farms: generateFakeFarms(NUM_FARMERS),
    farmers: generateFakeFarmers(NUM_FARMS)
  };
};
